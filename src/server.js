var server = "localhost";
var server_version = "1.0.0";
var port = 5000;
var serverUrl = server + ":" + port;

var clients = [];
var nicknames = [];
var timeoutsec = 90 * 1000;
var password = "";  // SENHA DO SERVIDOR, ALTERAR CASO QUEIRA TESTAR
var channels = {};

var commands = [
    "NICK", "USER", "QUIT", "PASS",
    "JOIN", "PART", "TOPIC", "NAMES", "LIST", "INVITE", "KICK", //MODE
    "PRIVMSG", "NOTICE",
    "MOTD",
    "WHO", "WHOIS", "KILL",
    "PING", "PONG",
    "LUSERS", "VERSION", "STATS", "TIME"
];

/**
 * Escreve na tela o log de um comando executado para o cliente
 * @param socket
 * @param code
 * @param message
 * @param param1
 */
function log(socket, code, message, client) {

    var strings = [];

    if (code) {
        strings = [':' + serverUrl, code, socket.nickname || '*', message];

    } else {
        strings = [':' + socket.nickname + '!' + socket.nickname + '@' + server, message]
    }

    var selectedClient = client || socket;
    selectedClient.write(strings.join(" ") + "\n");

}

var functions = {
    
    /**
     * Usuário fornece uma senha para entrar no servidor
     * @param socket
     * @param args
     */
     PASS: function(socket, args) {
         let pass = args[0];
         
         if (password == ""){
             log(socket, 'PASS', 'Não existe senha no servidor.');
             return;
         }
         
         if (!pass) {
             log(socket, 461, "PASS  :Not enough parameters");
             return;
         }
         
         if (pass != password) {
             log(socket, 464, 'PASS :Password incorrect');
             return;
         }
         
         if (socket.auth) {
             log(socket, 462, "PASS :Unauthorized command (already registered)");
             return;
         }
         
         socket.auth = true;
     },

    /**
     * Altera o nick de um usuário
     * @param socket
     * @param args
     */
    NICK: function (socket, args) {

        var nickname = args[0];
        
        if (password && !socket.auth) {
            log(socket, 'NICK', 'O Servidor é protegido com senha!');
            return;
        }

        if (nickname) {

            // Checar se é o nickname válido
            if (nickname.match(/^[0-9a-zA-Z]+$/)) {

                // Nick disponível
                if (nicknames.indexOf(nickname) == -1) {

                    // Remover nickname do socket, caso exista
                    if (socket.nickname && nicknames.indexOf(socket.nickname) >= 0) {
                        nicknames.splice(nicknames.indexOf(socket.nickname), 1);
                    }
    
                    socket.nickname = nickname;
                    nicknames.push(nickname);

                } else {
                    log(socket, 433, nickname + " :Nickname is already in use");
                }

            } else {
                log(socket, 432, nickname + " :Erroneous nickname");
            }

        } else {
            log(socket, 431, ":No nickname given");
        }
    },

    /**
	 * Especifica username, hostname e realname de um novo usuário
	 * @param socket
	 * @param args
	 */
    USER: function (socket, args) {

        var username = args[0];
        var mode = args[1];
        var unused = args[2];
        var realname = args[3];

        if (socket.username) {
            log(socket, 462, ':Unauthorized command (already registered)');

        } else {

            if (username && mode && realname) {
                socket.username = username;
                socket.mode = mode;
                socket.realname = realname;

                // Executar MOTD assim que usuário se cadastrar
                if (socket.nickname) {
                    functions.MOTD(socket);
                }

            } else {
                log(socket, 462, 'USER :Not enough parameters');
            }
        }
    },

    /**
     * Encerra conexão do cliente
     * @param socket
     * @param args
     */
    QUIT: function (socket, args) {

        // Liberar nickname
        nicknames.splice(nicknames.indexOf(socket.nickname), 1);

        // Remover cliente dos canais
        var clientChannels = JSON.parse(JSON.stringify(socket.channels));
        clientChannels.forEach(function(channel) {
            functions.leaveChannel(socket, channel);
        });
        
        if (args) {
            socket.quit_message = args[0];
        }

        socket.end();

    },

    /**
     * Insere um cliente em um ou mais canais
     * @param socket
     * @param args
     */
    JOIN: function (socket, args) {

        var argsChannels = args[0] ? args[0].split(",") : [];
        var argsKeys = args[1] ? args[1].split(",") : [];

        if (argsChannels.length > 0) {

            // Sair de todos os canais
            if (argsChannels.length == 1 && argsChannels[0] == 0) {

                var oldChannels = JSON.parse(JSON.stringify(socket.channels));
                oldChannels.forEach(function (channel) {
                    functions.leaveChannel(socket, channel);
                });

            } else {

                var validChannels = [];
                var keys = [];

                argsChannels.forEach(function (channel, index) {

                    if (channel !== 0 && channel) {
                        validChannels.push(channel);
                        keys.push(argsKeys[index] || false);
                    }

                });

                validChannels.forEach(function (channel, index) {

                    if (channel.charAt(0) == '#') {

                        // Criar canal, caso não exista
                        if (channels[channel] === undefined) {
                            channels[channel] = { name: channel, key: keys[index], clients: [] };
                        }

                        // Verificar se o usuário já não pertence ao canal
                        if (socket.channels.indexOf(channel) == -1) {

                            // Verifica se há senha e se a mesma está correta e adiciona o cliente no canal
                            if (!channels[channel].key || keys[index] == channels[channel].key) {
                                
                                socket.channels.push(channel);
                                channels[channel].clients.push(socket);
                                
                                // Mandar mensagem para todos no canal
                                channels[channel].clients.forEach(function(client) {
                                    log(socket, false, 'JOIN :' + channel, client);
                                });

                                // Mostrar tópico, se existir
                                if (channels[channel].topic) {
                                    functions.channelTopic(socket, channels[channel]);
                                }
                                
                                // Toda vez que entrar em um canal, mostrar os usuários conectados a ele
                                functions.listChannelNames(socket, channel);
                                
                            } else {
                                log(socket, 475, channel + ' :Cannot join channel (+k)');
                            }
                        }

                    } else {
                        log(socket, 479, channel + ' :Illegal channel name');
                    }

                });
            }

        } else {
            log(socket, 461, ":Not enough parameters");
        }
    },

    /**
     * Comando executado para sair de um canal
     * @param socket
     * @param args
     */
    PART: function (socket, args) {

        if (args[0]) {

            var argsChannels = args[0].split(",");

            argsChannels.forEach(function (channel, index) {
                functions.leaveChannel(socket, channel, args[1]);
            });

        } else {
            log(socket, 461, ':Not enough parameters');
        }
    },

    /**
     * Remover um cliente de um canal
     * @param socket
     * @param channel
     * @param message
     */
    leaveChannel: function(socket, channel, message) {

        if (channels[channel]) {

            let channelIndexOnSocket = socket.channels.indexOf(channel);
            let socketIndexOnChannel;

            if (channelIndexOnSocket >= 0) {

                // Remover canal do cliente
                socket.channels.splice(channelIndexOnSocket, 1);

                // Remover cliente do canal
                channels[channel].clients.forEach(function(client, index) {
                    if (client.name == socket.name) socketIndexOnChannel = index; return false;
                });

                if (socketIndexOnChannel >= 0) {

                    // Mandar mensagem para todos no canal
                    channels[channel].clients.forEach(function(client) {
                        log(socket, false, 'PART ' + channel + (message ? (' :' + message) : ''), client);
                    });

                    channels[channel].clients.splice(socketIndexOnChannel, 1);

                    if (channels[channel].clients.length == 0) {
                        delete channels[channel];
                    }
                }

            } else {
                log(socket, 442, channel + " :You're not on that channel");
            }

        } else {
            log(socket, 403, channel + ' :No such channel');
        }
 
    },


    /**
     * Visualizar ou atribuir um tópico ao canal
     */
    TOPIC: function (socket, args) {
        
        var channel = args[0];
        var topic = args[1];

        if (channel) {

            var sc = channels[channel]; // selected channel
            if (sc) {

                if (topic) {

                    if (socket.channels.indexOf(channel) >= 0) {

                        // Tem permissão para alterar o tópico
                        if (sc.clients[0].nickname == socket.nickname) {

                            if (topic == ':') {
                                delete channels[channel].topic;
                                delete channels[channel].topic_time;
                                delete channels[channel].topic_who;

                            } else {
                                channels[channel].topic = topic;
                                channels[channel].topic_time = Math.floor(Date.now() / 1000);
                                channels[channel].topic_who = socket;
                            }

                            // Avisar aos clientes no canal
                            sc.clients.forEach(function(client) {
                                log(socket, false, 'TOPIC ' + channel + ' ' + (topic == ':' ? topic : (':' + topic)), client);
                            });
    
                        } else {
                            log(socket, 482, channel + ' :You are not channel operator');
                        }
                    
                    } else {
                        log(socket, 442, channel + ' :You are not on that channel');
                    }

                // Visualizar tópico atual
                } else {

                    if (sc.topic) {
                        functions.channelTopic(socket, sc);

                    } else {
                        log(socket, 331, channel + ' :No topic is set.');
                    }
                }

            } else {
                log(socket, 403, channel + ' :No such channel');   
            }

        } else {
            log(socket, 461, ':Not enough parameters');
        }

    },

    /**
     * Mostrar o tópico de um canal
     * @param socket
     * @param channel
     */
    channelTopic: function(socket, channel) {

        var who = channel.topic_who;
        var message = channel.name + ' ' + (who.nickname+'!'+who.username+'@'+server) + ' ' + channel.topic_time;

        log(socket, 332, channel.name + ' :' + channel.topic);
        log(socket, 333, message);

    },


    /**
     * Visualizar todos os usuários em um ou mais canais
     * @param socket
     * @param args
     */
    NAMES: function (socket, args) {

        var stringChannels = args[0];
        var arrChannels = args[0] ? args[0].split(",") : [];

        if (stringChannels && arrChannels.length > 0) {

            arrChannels.forEach(function(channel) {

                // Checar se o usuário está no canal
                if (socket.channels.indexOf(channel) !== -1) {
                    functions.listChannelNames(socket, channel);
                
                } else {
                    log(socket, 366, channel + ' :End of /NAMES list.');       
                }

            });
        
        } else {
            log(socket, 366, "* :End of /NAMES list.");
        }
    },

    /**
     * Visualizar todos os usuários em um canal
     * @param socket
     * @param channel
     */
    listChannelNames: function(socket, channel) {

        var selectedChannel = channels[channel];
        if (selectedChannel && selectedChannel.clients.length > 0) {

            var nicknames = [];
            selectedChannel.clients.forEach(function(client, index) {
                nicknames.push(index == 0 ? ('@' + client.nickname) : client.nickname);
            });

            var message = "= " + channel + " :" + nicknames.reverse().join(" ");
            log(socket, 353, message);
            log(socket, 366, channel + ' :End of /NAMES list.');

        } else {
            log(socket, 366, channel + ' :End of /NAMES list.');
        }
    },


    /**
     * Visualizar as informações dos canais
     * @param socket
     * @param args
     */
    LIST: function (socket, args) {
        
        var stringChannels = args[0];
        var arrChannels = args[0] ? args[0].split(",") : [];

        log(socket, 321, 'Channel :Users Name');

        if (stringChannels && arrChannels.length > 0) {

            arrChannels.forEach(function(channel) {
                functions.postList(socket, channel);
            });
        
        } else {
            
            let keys = Object.keys(channels);
            keys.forEach(function(channel) {
                functions.postList(socket, channel);
            });

        }

        log(socket, 323, ':End of /LIST');

    },

    /**
     * Channel mode command
     * @param socket
     * @param args
     */
    MODE: function(socket, args) {
        // TODO
    },

     /**
     * Visualizar todos os usuários em um canal
     * @param socket
     * @param channel
     */
    postList: function(socket, channel) {

        var selectedChannel = channels[channel];
        
        if (selectedChannel) {
            log(socket, 322, channel + ' ' + selectedChannel.clients.length + ' :[+nt]');
        }

    },

    /**
     * Convida um usuário para o canal
     * @param socket
     * @param args
     */
    INVITE: function(socket, args) {

        var nick = args[0];
        var channel = args[1];

        if (channel && nick) {

            var selectedClient = false;
            var selectedChannel = channels[channel];

            clients.forEach(function(client) {
                if (client.nickname == nick) selectedClient = client;
            });

            if (selectedClient) {

                if (selectedChannel) {

                    if (socket.channels.indexOf(channel) >= 0) {

                        if (selectedChannel.clients[0].nickname == socket.nickname) {
                            
                            var inviteOnChannel = false;

                            selectedChannel.clients.forEach(function(client) {
                                if (client.nickname == nick) inviteOnChannel = true;
                            });

                            if (inviteOnChannel) {
                                log(socket, 443, nick + " " + channel + " :is already on channel");
                            
                            } else {
                                log(socket, 341, nick + " " + channel);
                                log(socket, false, "INVITE " + nick + " :" + channel, selectedClient);
                            }

                        } else {
                            log(socket, 482, channel + " :You are not channel operator");
                        }
                        
                    } else {
                        log(socket, 442, channel + " :You are not on that channel");
                    }

                } else {
                    log(socket, 403, channel + " :No such channel");
                }
                
            } else {
                log(socket, 401, nick + " :No suck nick/channel");
            }
            
        } else {
            log(socket, 461, ':Not enough parameters');
        }
    },

    /**
     * Expulsar um cliente de algum canal
     * @param socket
     * @param args
     */
    KICK: function(socket, args) {

        var channel = args[0];
        var nick = args[1];
        var message = args[2] ? args[2] : socket.nickname;

        if (channel && nick) {

            var selectedClient = false;
            var selectedChannel = channels[channel];

            clients.forEach(function(client) {
                if (client.nickname == nick) selectedClient = client;
            });

            if (selectedChannel) {

                if (socket.channels.indexOf(channel) >= 0) {

                    if (selectedClient) {

                        if (selectedChannel.clients[0].nickname == socket.nickname) {

                            var kickOnChannel = -1;

                            selectedChannel.clients.forEach(function(client, index) {
                                if (client.nickname == nick) kickOnChannel = index;
                            });

                            console.log("kick on channel", kickOnChannel);

                            if (kickOnChannel >= 0) {
                                log(socket, 341, nick + " " + channel);
                                log(socket, false, "KICK " + channel + " :" + nick + " :" + message, selectedClient);
                                
                                // Remover usuário do canal
                                selectedChannel.clients.splice(kickOnChannel, 1);

                                // Remover canal do usuário
                                var index = selectedClient.channels.indexOf(channel);
                                selectedClient.channels.splice(index, 1);

                            } else {
                                log(socket, 443, nick + " " + channel + " :They aren't on that channel");
                            }

                        } else {
                            log(socket, 482, channel + " :You are not channel operator");
                        }
                        
                    } else {
                        log(socket, 401, nick + " :No suck nick/channel");
                    }

                } else {
                    log(socket, 442, channel + " :You are not on that channel");
                }

            } else {
                log(socket, 403, channel + " :No such channel");
            }

        } else {
            log(socket, 461, ':Not enough parameters');
        }
    },

    PRIVMSG: function (socket, args) {

        var target = args[0];
        var message = args[1];

        if (target) {

            if (message) {

                // Mensagem para server
                if (target.charAt(0) == "$") {

                // Mensagem para host
                } else if (target.charAt(0) == "#") {

                // Mensagem para nickname
                } else {

                    var search = { nickname: "", username: "", host: "", server: "" };
                    var active = "nickname";

                    for (var i=0; i<target.length; i++) {

                        var char = target.charAt(i);

                        if (char == "!") {
                            active = "username";

                        } else if (char == "@") {
                            active = "server";

                        } else if (char == "%") {
                            active = "host";

                        } else {
                            search[active] = search[active].concat(char);
                        }
                    }

                    clients.forEach(function(client) {

                        var nicknameChecked = client.nickname == search.nickname;
                        var usernameChecked = !search.username || client.username == search.username;
                        var hostChecked = !search.host || client.host == search.host || true;
                        var serverChecked = !search.server || client.server == search.server || true;

                        if (nicknameChecked && usernameChecked && hostChecked && serverChecked) {
                            log(socket, false, 'PRIVMSG ' + client.nickname + ' :' + message, client);
                        }

                    });

                }

            } else {
                log(socket, 412, ':No text to send');
            }

        } else {
            log(socket, 411, ':No recipient given (PRIVMSG)');
        }

    },
    
    // Bastante similar ao PRIVMSG, cabe ao cliente tratar de forma diferente.
    NOTICE: function(socket, args) {
        
        var target = args[0];
        var message = args[1];

        if (target) {

            if (message) {

                // Mensagem para server
                if (target.charAt(0) == "$") {

                // Mensagem para host
                } else if (target.charAt(0) == "#") {

                // Mensagem para nickname
                } else {

                    var search = { nickname: "", username: "", host: "", server: "" };
                    var active = "nickname";

                    for (var i=0; i<target.length; i++) {

                        var char = target.charAt(i);

                        if (char == "!") {
                            active = "username";

                        } else if (char == "@") {
                            active = "server";

                        } else if (char == "%") {
                            active = "host";

                        } else {
                            search[active] = search[active].concat(char);
                        }
                    }

                    clients.forEach(function(client) {

                        var nicknameChecked = client.nickname == search.nickname;
                        var usernameChecked = !search.username || client.username == search.username;
                        var hostChecked = !search.host || client.host == search.host || true;
                        var serverChecked = !search.server || client.server == search.server || true;

                        if (nicknameChecked && usernameChecked && hostChecked && serverChecked) {
                            log(socket, false, 'NOTICE ' + ' :' + message, client);
                        }

                    });
                }

            } else {
                log(socket, 412, ':No text to send');
            }

        } else {
            log(socket, 411, ':No recipient given (NOTICE)');
        }

    },
    
    /**
     * Comando MOTD
     * @param socket
     * @param args
     */
    MOTD: function (socket, args) {
        log(socket, 375, ":- " + server + " Message of the Day - ");
        log(socket, 372, ":- Melhor chat IRC da UFG");
	    log(socket, 376, ":End of MOTD command");
    },

    /**
     * Realiza uma busca na lista de clientes do servidor
     * @param socket
     * @param args
     */
    WHO: function (socket, args) {
        
        var mask = args[0];

        if (mask && mask != 0) {

            var selectedClientIndex = false;
            var selectedClient = false;

            // Pelo IRCD-Hybrid, a função busca apenas pelo nick
            clients.forEach(function(client, index) {

                if (client.nickname && client.username && client.nickname == mask.toLowerCase()) {
                    selectedClient = client;
                    selectedClientIndex = index;
                }

            });

            if (selectedClient) {
                log(socket, 352, functions.whoClientInfo(selectedClient, false, selectedClientIndex == 0));
            }
        
        // Mostrar clientes que estão no mesmo canal que você
        } else {

            var sc = socket.channels; // socket channels
            var friends = [];

            clients.forEach(function(client, index) {

                var cc = client.channels;
                var isFriend = false;

                client.channels.forEach(function(channel) {
                    if (sc.indexOf(channel) >= 0) isFriend = true;
                });

                if (isFriend) friends.push(client);

            });

            friends.forEach(function(client) {
                log(socket, 352, functions.whoClientInfo(client, true, false));
            });
        }

        log(socket, 315, (mask ? mask + ' ' : '') + ':End of /WHO list.');

    },

    /**
     * Retorna as informações de determinado cliente
     * @param client
     * @param ignoreChannels
     * @param firstClient
     */
    whoClientInfo: function(client, ignoreChannels, firstClient) {

        var strings = [];

        var cc = client.channels; // client channels
        
        if (ignoreChannels == false && cc && cc.length > 0) {
            strings.push(cc[cc.length - 1]);
        
        } else {
            strings.push('*');
        }

        strings.push(client.username);
        strings.push(server); // host
        strings.push(server); // server
        strings.push(client.nickname);
        strings.push('H' + (firstClient ? '@' : ''));
        strings.push(':0'); // hopcount ??
        strings.push(client.realname);

        return strings.join(" ");
    },


    /**
     * Realiza um ping no servidor local
     * @param socket
     * @param args
     */
    PING: function (socket, args) {
        
        var token = args[0];
        
        if (token) {
            functions.PONG(socket, "localhost:"+port, token);
            
        } else {
            socket.write("ERRO: Token não especificado.\n");
        }
    },

    /**
     * Utilizado como forma de confirmação de um ping realizado anteriormente
     */
    PONG: function (socket, who, token) {
        
        // Sempre que for chamada, essa função irá definir o cliente como ativo
        // e resetar seu timeout
        socket.active = true;
        socket.setTimeout(timeoutsec);
        
        // Se a mensagem vier com todos os parâmetros, significa que veio do servidor
        // para ser enviada para um cliente confirmando o PING que ele havia executado
        if (who && token) {
            //socket.write("PONG "+who+": "+token+"\n");
        }
    },

    /**
     * Utilizado para retornar status da conexão
     */
    LUSERS: function (socket, args){

        //Server unico implementado, leitura de parâmetros para outros servers será implementada na proxima versão
        log(socket, false, "There are " + clients.length + " on 1 server\r\n");
        log(socket, false, channels.length + ": channels formed\r\n");

    },

    /**
     * Utilizado para retornar a versão atual do servidor
     */
    VERSION: function (socket, args){

        //Server unico implementado, leitura de parâmetros para outros servers será implementada na proxima versão
        log(socket, false, "Version: " + server_version + "\r\n");
        
    },

    STATS: function (socket, args){

    },

    /**
     * Utilizado para retornar a hora local
     */
    TIME: function (socket, args){

        var local_time;

        if (!Date.now) {
            Date.now = function now() {
            return new Date().getTime();
            };
        }

        local_time = Date.now();

        //Server unico implementado, leitura de parâmetros para outros servers será implementada na proxima versão
        log(socket, false, "Time: " + local_time);
    },

    /**
     * Comando WHOIS
     * @param socket
     * @param args
     */
    WHOIS: function (socket, args) {
        
        var mask = args[0];
        var checkOperator = args[1] == "o";

        if (mask) {

            clients.forEach(function(client) {

                var usernameCheck = client.username && client.username.indexOf(mask) >= 0;
                var nicknameCheck = client.nickname && client.nickname.indexOf(mask) >= 0;
                var realnameCheck = client.realname && client.realname.indexOf(mask) >= 0;

                if (usernameCheck || nicknameCheck || realnameCheck) {
                    socket.write(311 , client.nickname , client.username , server ,client.realname , "\n");
                }

            });

        } else {
            socket.write("ERRO: ERR_NONICKNAMEGIVEN \n");
        }   
    },

    /**
     * Comando KILL
     * @param socket
     * @param args
     */
	KILL: function(socket, args) {

		var nickname = args[o];
		var comment = args[1];
		
		if (nickname && comment) {
			// Liberar nickname
			nicknames.splice(nickname.indexOf(socket.nickname), 1);
			socket.write(comment);
			socket.end();
  
        } else {
            socket.write("ERRO: ERR_NEEDMOREPARAMS ");
        }
	},

}

/**
 * Retorna os parâmetros em uma string
 * @param string 
 */
var getArgs = function (string) {

    var parts = string.split(" ");
    var args = [];

    var tmp = [];
    var singleWord = true;

    parts.forEach(function (part, index) {

        if (part.charAt(0) == ":") {

            part = part.slice(1);
            singleWord = false;

            if (tmp.length > 0) {
                args.push(tmp.join(" "));
            }

            tmp = [];
        }

        if (singleWord) {
            args.push(part);

        } else {

            tmp.push(part);

            if (index == parts.length - 1) {
                args.push(tmp.join(" "));
            }
        }

    });

    return args;
}

/**
 * Analisa a mensagem digitada pelo usuário e executa o comando específicado
 * @param socket
 * @param buffer 
 */
var execute = function (socket, buffer) {

    var string = String(buffer).trim();
    if (string.length > 0) {

        var args = getArgs(string);
        var command = args[0];
    
        if (socket.nickname || socket.username || command == "USER" || command == "NICK" || command == "QUIT" || command == "PASS") {
    
            // Comando inválido
            if (commands.indexOf(command) == -1) {
                log(socket, 421, command + ' :Unknown command');
        
            // Executa a função selecionada
            } else {
                args.shift();
                functions[command](socket, args);
            }
    
        // Erro de ciclo
        } else {
            log(socket, 451, ':You have not registered');
        }
    }

    if (command !== "PONG") functions.PONG(socket);
}

/**
 * Checar inatividade de conexão de algum cliente
 * @param socket 
 */
var checkConnection = function(socket) {
    
    if (socket.active) {
        functions.PING(socket, "localhost:"+port);
        socket.active = false;
        socket.setTimeout(timeoutsec);
        
    // Finalizar conexão por inatividade
    } else {
        socket.write("ATENÇÃO: Desconectado por inatividade.\n");
        functions.QUIT(socket);
    }
}

net = require('net');
net.createServer(function (socket) {

    socket.name = socket.remoteAddress + ":" + socket.remotePort;
    socket.active = false;
    socket.channels = [];
    socket.setTimeout(timeoutsec);  


    // Inserir cliente na lista
    clients.push(socket);

    // Função executada sempre que o cliente realiza uma requisição
    socket.on('data', function (buffer) {
        execute(socket, buffer);
    });

    // Função executada sempre que um cliente atinge o timeout
    socket.on("timeout", function() {
        checkConnection(socket);
    });

    // Função executada sempre que um cliente encerra a conexão
    socket.on('end', function (message) {
        
        clients.splice(clients.indexOf(socket), 1);

        // Remover cliente dos canais
        var clientChannels = JSON.parse(JSON.stringify(socket.channels));
        clientChannels.forEach(function(channel) {
            functions.leaveChannel(socket, channel);
        });

    });

}).listen(port);